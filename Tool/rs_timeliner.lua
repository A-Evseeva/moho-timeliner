-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "RS_Timeliner"

-- **************************************************
-- General information about this script
-- **************************************************

RS_Timeliner = {}

function RS_Timeliner:Name()
	return 'Timeliner'
end

function RS_Timeliner:Version()
	return '1.0'
end

function RS_Timeliner:UILabel()
	return 'Timeliner'
end

function RS_Timeliner:Creator()
	return 'Revival Studio'
end

function RS_Timeliner:Description()
	return ''
end

function RS_Timeliner:ColorizeIcon()
	return true
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function RS_Timeliner:IsRelevant(moho)
	return true
end

function RS_Timeliner:IsEnabled(moho)
	return true
end

-- **************************************************
-- Recurring Values
-- **************************************************

RS_Timeliner.childLayers = true
RS_Timeliner.bonesMode = false
RS_Timeliner.grabMode = false
RS_Timeliner.grabFrames = 0
RS_Timeliner.grabStart = nil
RS_Timeliner.hBorder = 0.05
RS_Timeliner.vBorder = 0.3

-- **************************************************
-- Prefs
-- **************************************************

function RS_Timeliner:LoadPrefs(prefs)
	self.childLayers = prefs:GetBool("RS_Timeliner.childLayers", false)
	self.bonesMode = prefs:GetBool("RS_Timeliner.bonesMode", true)
	self.bonesLagMode = prefs:GetBool("RS_Timeliner.bonesLagMode", false)
end

function RS_Timeliner:SavePrefs(prefs)
	prefs:SetBool("RS_Timeliner.childLayers", self.childLayers)
	prefs:SetBool("RS_Timeliner.bonesMode", self.bonesMode)
	prefs:SetBool("RS_Timeliner.bonesLagMode", self.bonesLagMode)
end

function RS_Timeliner:ResetPrefs()
	self.childLayers = false
	self.bonesMode = true
	self.bonesLagMode = false
end

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function RS_Timeliner:NonDragMouseMove()
	return true
end

function RS_Timeliner:ProcessChildBone(moho, skel, parentBoneID, frame)
	print("Called ProcessChildBone with parent ", skel:Bone(parentBoneID):Name(), " at frame ", frame)
	local start, finish = self:GetFrameInterval(moho)
	local channelsArray = {'fAnimAngle', 'fAnimPos', 'fAnimScale', 'fFlipH', 'fFlipV', 'fAnimParent', 'fTargetBone'}
	for b=0, skel:CountBones()-1 do
		local nextBone = skel:Bone(b)
		if nextBone.fAnimParent:GetValue(frame) == parentBoneID then
			local frameToCall = finish + 1
			for i, channelName in pairs(channelsArray) do
				channel = nextBone[channelName]
				for f = frame, channel:Duration() do
					if channel:HasKey(f) then
						if f < frameToCall then frameToCall = f end
					end
				end
			end
			if frameToCall <= finish then 
				for i, channelName in pairs(channelsArray) do
					channel = nextBone[channelName]		
					if channel:HasKey(frameToCall) then channel:SetKeySelected(frameToCall, true) end
				end
				self:ProcessChildBone(moho, skel, b, frameToCall)	
			end
		end
	end

end

function RS_Timeliner:OnMouseDown(moho, mouseEvent)
	
	if self.grabMode then
		self.grabMode = false
		
		local step, start, finish = 1, self:GetFrameInterval(moho)
		if self.grabFrames > 0 then step, start, finish = -1, finish, start end
		local selectedKeys = {}
		for frame = start, finish, step do
			--local offsetFrame = moho.layer:TotalTimingOffset() + frame
			--for subId, id, channel, chInfo in AE_Utilities:IterateAllChannels(moho, moho.layer) do
			for channel, offset, isPrimary in self:IterateChannels(moho) do
			local offsetFrame = offset + frame
				if channel:IsKeySelected(offsetFrame) then
					local derivedChannel = AE_Utilities:GetDerivedChannel(moho, channel)
					table.insert(selectedKeys, {channel=derivedChannel, frame=offsetFrame})
				end
			end
		end
		
		local interp = MOHO.InterpSetting:new_local()
		for i, v in pairs(selectedKeys) do
			v.channel:GetKeyInterp(v.frame, interp)
			v.channel:SetValue(v.frame + self.grabFrames, v.channel:GetValue(v.frame))
			v.channel:SetKeyInterp(v.frame + self.grabFrames, interp)
			v.channel:DeleteKey(v.frame)
		end
		return
	end
	
	--iterate all channels to select or deselect corresponding keys
	local what = self:DrawMe(moho, mouseEvent.view, mouseEvent.pt)
	if what then 
		--local offsetFrame = moho.layer:TotalTimingOffset() + what.frame
		--for subId, id, channel, chInfo in AE_Utilities:IterateAllChannels(moho, moho.layer) do
		for channel, offset, isPrimary in self:IterateChannels(moho) do
			local offsetFrame = offset + what.frame
			if channel:HasKey(offsetFrame)and isPrimary == what.up then
				channel:SetKeySelected(offsetFrame, what.selected ~= 0)
				if self.bonesMode and self.bonesLagMode and isPrimary and what.selected ~= 0 then
					local skel = moho:Skeleton()
					local selID = skel:SelectedBoneID()
					self:ProcessChildBone(moho, skel, selID, what.frame)
				end
			end			
		end
	end
	
	
	--TODO: do same for child layers 
	--TODO: do same for bones mode
	--TODO: check shift modifier and select all the keys begining from left (or right) border of allready selected and ending with current clicked
	--TODO: ignore hidden bones (?)
	
end

function RS_Timeliner:OnMouseMoved(moho, mouseEvent)
	--if in grab mode
	if self.grabMode then
		if not self.grabStart then 
			self.grabStart = LM.Vector2:new_local()
			self.grabStart:Set(mouseEvent.pt.x, mouseEvent.pt.y)
		end
		--calculate number of frame grabed
		local start, finish = self:GetFrameInterval(moho)
		local frameLength = mouseEvent.view:Width()*(1 - 2*self.hBorder)/(finish - start)
		local grabFrames = math.floor((mouseEvent.pt.x - self.grabStart.x)/frameLength)
		if grabFrames ~= self.grabFrames then 
			self.grabFrames = grabFrames
			MOHO.Redraw()
		end
	end
end

function RS_Timeliner:OnMouseUp(moho, mouseEvent)
	
end

function RS_Timeliner:OnKeyDown(moho, keyEvent)
	--test press G key 
	if not self.grabMode and keyEvent.keyCode == 103 then
		self.grabMode = true
		self.grabStart = nil
		self.grabFrames = 0
	end
	
	--TODO: also process C or X key for copy or cut (selected keys) and paste (into current frame(s))
	--TODO: also process DEL key to delete selected keys
	--TODO: also maybe invent some hotkeys for step/linear and 1/2 interpolation, or for freeze (or something else from keytools functional)
	--TODO: also jump between keyframes
	--TODO: also modify key selection someway
end

function RS_Timeliner:OnKeyUp(moho, keyEvent)

end

function RS_Timeliner:OnInputDeviceEvent(moho, deviceEvent)

end

function RS_Timeliner:GetFrameInterval(moho)
	local doc = moho.document
	local start = (MOHO.MohoGlobals.PlayStart > 0) and MOHO.MohoGlobals.PlayStart or doc:StartFrame()
	local finish = (MOHO.MohoGlobals.PlayEnd > 0) and MOHO.MohoGlobals.PlayEnd or doc:EndFrame()	
	return start, finish
end

function RS_Timeliner:DrawMarker(moho, g, tmlY, frmX, selected, secondary)
	local vector = LM.Vector2:new_local()
	vector.x = frmX
	vector.y = tmlY - 20
	if secondary then vector.y = tmlY + 20 end
	g:FrameCircle(vector, 6)
	if selected == 0 then g:FillCircle(vector, 6)
	elseif selected > 0 then g:FillCircle(vector, 3)
	end
	local dist = -14
	if secondary then dist = 14 end
	g:DrawLine(frmX, tmlY + dist, frmX, tmlY)
end

function RS_Timeliner:DrawMe(moho, view, pt)
	if self.bonesMode and not moho:Skeleton() then return end

	local w = view:Width()
	local h = view:Height()
	local beginX = w*self.hBorder
	local endX = w*(1 - self.hBorder)
	local tmlY = h*(1 - self.vBorder)
	local start, finish = self:GetFrameInterval(moho)

	
	--calculate number of frame grabed
	if self.grabMode and pt then
		local grabFrames = math.floor((finish - start) * (pt.x - self.grabStart.x)/(endX - beginX))
		if grabFrames == self.grabFrames then return 
		else self.grabFrames = grabFrames
		end
	end		
	
	local g = view:Graphics()
	local returnClick = nil

	g:Push()
	m = g:CurrentTransform()
	m:Invert()	
	g:ApplyMatrix(m)	
	
	--draw the horisontal line
	g:SetColor(255, 0, 0, 255)
	g:DrawFatLine(4, beginX, tmlY, endX, tmlY)

	
	if moho.frame >= start and moho.frame <= finish then 
		g:SetColor(255, 255, 0, 255)
		--calculate current frame X
		local curFrameFactor = (moho.frame - start) / (finish - start)
		local curFrameX = (endX - beginX) * curFrameFactor + beginX
		local curFrameHeight = 20
		g:DrawFatLine(4, curFrameX, tmlY - curFrameHeight, curFrameX, tmlY + curFrameHeight)
		g:SetColor(255, 0, 0, 255)
	end
	
	local keyPosVector = LM.Vector2:new_local()
	
	for frame = start, finish do
		--calculate X for iterated frame
		frameX = beginX + (endX - beginX) * (frame - start) / (finish - start)
		keyPosVector.x = frameX
		local exists = false
		local selected = -1
		local child_exists = false
		local child_selected = -1		
		--local offsetFrame = moho.layer:TotalTimingOffset() + frame
		--for subId, id, channel, chInfo in AE_Utilities:IterateAllChannels(moho, moho.layer) do
		for channel, offset, isPrimary in self:IterateChannels(moho) do
			local offsetFrame = offset + frame
			if channel:HasKey(offsetFrame)then
				if isPrimary then exists = true else child_exists = true end
				if channel:IsKeySelected(offsetFrame) then 
					if selected < 0 and isPrimary then selected = 0 end 
					if child_selected < 0 and not isPrimary then child_selected = 0 end 
				else 
					if selected == 0 and isPrimary then 
						selected = 1 
						if child_selected > 0 then break end
					end
					if child_selected == 0 and not isPrimary then
						child_selected = 1
						if selected > 0 then break end
					end
				end
			end			
		end		
		if exists or child_exists then
			if exists then self:DrawMarker(moho, g, tmlY, frameX, selected) end
			if child_exists then self:DrawMarker(moho, g, tmlY, frameX, child_selected, true) end
			
			if (selected >= 0 or child_selected >=0) and self.grabMode then
				local grabFrame = frame + self.grabFrames
				if grabFrame >= start and grabFrame <= finish then
					--calculate X for grabFrame
					local grabX = beginX + (endX - beginX) * (grabFrame - start) / (finish - start)
					g:SetColor(0, 0, 255, 255)
					if selected >=0 then self:DrawMarker(moho, g, tmlY, grabX, 2) end
					if child_selected >=0 then self:DrawMarker(moho, g, tmlY, grabX, 2, true) end
					g:SetColor(255, 0, 0, 255)
				end
			end
			
			if pt and not self.grabMode then
				keyPosVector:Set(frameX - pt.x, tmlY - 20 - pt.y)
				if  keyPosVector:Mag() < 10 then returnClick = {up=true, selected=selected, frame=frame} end
				keyPosVector:Set(frameX - pt.x, tmlY + 20 - pt.y)
				if  keyPosVector:Mag() < 10 then returnClick = {up=false, selected=child_selected, frame=frame} end
			end			
		end
		--TODO: iterate child layers if needed  and draw their keys below the line, also filled for selected ones
		--TODO: ignore some layers and their children by (color or lock or tag?)
		--TODO: ignore hidden bones (?)
	end

	--TODO: provide alternative iteration on (current) bone mode	
	
	g:Pop()

	return returnClick
	

end

-- **************************************************
-- Custom methods
-- **************************************************

function RS_Timeliner:IterateBones(moho)
	--TODO: if in bone mode return something absolutely different
	local skel = moho:Skeleton()
	if not skel then return nil end
	local offset = (moho.layer:ControllingBoneLayer() or moho.layer):TotalTimingOffset()
	--iterate selected bone and its children as the lower Timeliner
	local bonesArray = {}
	for i=0, skel:CountBones() - 1 do
		local bone = skel:Bone(i)
		if bone.fSelected then table.insert(bonesArray, {bone=bone, boneID=i, selected=true})
		elseif skel:IsAncestorSelected(i) then table.insert(bonesArray, {bone=bone, boneID=i, selected=false})
		end
	end
	local channelsArray = {'fAnimAngle', 'fAnimPos', 'fAnimScale', 'fFlipH', 'fFlipV', 'fAnimParent', 'fTargetBone'}
	local boneIterator = 1
	local channelIterator = 0
	return function()
		channelIterator = channelIterator + 1
		if channelIterator > #channelsArray then
			boneIterator = boneIterator + 1
			if boneIterator > #bonesArray then return nil end
			channelIterator = 1
		end
		local channel = bonesArray[boneIterator].bone[channelsArray[channelIterator]]
		channel = AE_Utilities:GetDerivedChannel(moho, channel)
		return channel, offset, bonesArray[boneIterator].selected
	end
end

function RS_Timeliner:IterateChannels(moho)

	if self.bonesMode then return self:IterateBones(moho) end

	-- iterate selected layer or all layers or all layers excluding something
	-- first collect the layers then iterate them	
	local currentLayer = moho.layer
	local layersCollection = {currentLayer}
	--TODO: collect layers excluding what to exclude
	if self.childLayers then
		for i, layer in AE_Utilities:IterateAllLayers(moho) do 
			if layer ~= currentLayer and layer:IsAncestorSelected() then
				--For now the next line hangs Moho forever
				--table.insert(layersCollection, layer)
			end
		end
	end
	local layersCounter = 1
	local nextLayer = layersCollection[layersCounter]
	-- for each of layers iterate its channels using AE_Utilities
	local channelIterator, condition, initCounter = AE_Utilities:IterateAllChannels(moho, nextLayer)
	
	return function()
		local subCounter, channelCounter, channel, chInfo = channelIterator(condition, initCounter)
		if not chInfo then
			layersCounter = layersCounter + 1
			if layersCounter > #layersCollection then 
				return nil
			end
			nextLayer = layersCollection[layersCounter]
			channelIterator, condition, initCounter = AE_Utilities:IterateAllChannels(moho, nextLayer)
			subCounter, channelCounter, channel, chInfo = channelIterator(condition, initCounter)
		end
		initCounter = subCounter
		return channel, nextLayer:TotalTimingOffset(), (nextLayer == currentLayer)
	end
	


end



-- **************************************************
-- Tool Panel Layout
-- **************************************************

RS_Timeliner.CHILDLAYERS = MOHO.MSG_BASE
RS_Timeliner.BONESMODE = MOHO.MSG_BASE + 1
RS_Timeliner.BONESLAGMODE = MOHO.MSG_BASE + 2

function RS_Timeliner:DoLayout(moho, layout)
	self.bonesModeCheck = LM.GUI.CheckBox('Bones mode', self.BONESMODE)
	layout:AddChild(self.bonesModeCheck, LM.GUI.ALIGN_LEFT, 0)
	
	self.bonesLagModeCheck = LM.GUI.CheckBox('Bones lag selection mode', self.BONESLAGMODE)
	layout:AddChild(self.bonesLagModeCheck, LM.GUI.ALIGN_LEFT, 0)	

	self.childLayersCheck = LM.GUI.CheckBox('Child layers', self.CHILDLAYERS)
	layout:AddChild(self.childLayersCheck, LM.GUI.ALIGN_LEFT, 0)
	
	--TODO: add something to use as ignore by for layers

	--TODO: key properties controls
	

end

function RS_Timeliner:UpdateWidgets(moho)
	RS_Timeliner.childLayersCheck:SetValue(self.childLayers)
	RS_Timeliner.bonesModeCheck:SetValue(self.bonesMode)
	self.childLayersCheck:Enable(not self.bonesModeCheck:Value())
	self.bonesLagModeCheck:Enable(self.bonesModeCheck:Value())	
end

function RS_Timeliner:HandleMessage(moho, view, msg)
	if msg == self.CHILDLAYERS then
		self.childLayers = self.childLayersCheck:Value()
	elseif msg == self.BONESMODE then
		self.bonesMode = self.bonesModeCheck:Value()
	else
		self.bonesLagMode = self.bonesLagModeCheck:Value()
	end
	self:UpdateWidgets(moho)
end
